<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('layouts.site.partials.htmlheader')
@show


<body data-spy="scroll" data-target="#navigation" data-offset="50">

<div id="app" v-cloak>

  @include('layouts.site.partials.mainheader')

   <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->


    
        @include('layouts.site.partials.footer')


</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

@section('scripts')
    @include('layouts.site.partials.scripts')
@show



</body>
</html>
