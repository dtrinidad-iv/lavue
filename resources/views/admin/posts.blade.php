@extends('layouts.admin.app')

@section('htmlheader_title')
   Manage Posts
@endsection



@section('content-header')
 <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Manage Posts')
        <small>@yield('contentheader_description','')</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Posts</li>
    </ol>
</section>
@endsection


@section('main-content')
  <posts></posts>
@endsection
