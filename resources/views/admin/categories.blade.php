@extends('layouts.admin.app')

@section('htmlheader_title')
   Manage Categories
@endsection



@section('content-header')
 <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Manage Categories')
        <small>@yield('contentheader_description','')</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Categories</li>
    </ol>
</section>
@endsection


@section('main-content')
<!--  <my-vuetable></my-vuetable><br><br> -->
  <categories></categories>
@endsection
