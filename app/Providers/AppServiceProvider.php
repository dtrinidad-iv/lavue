<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
 
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\EloquentCategory;
 
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\EloquentPost;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }

        $this->app->singleton(CategoryRepository::class, EloquentCategory::class);
        $this->app->singleton(PostRepository::class, EloquentPost::class);
    }
}
