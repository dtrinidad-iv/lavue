<?php

namespace App\Repositories\Category;

use Illuminate\Http\Request;
use App\Models\Category;
use Image;

class EloquentCategory implements CategoryRepository
{
	/**
	 * @var $model
	 */
	private $model;

	/**
	 * EloquentCategory constructor.
	 *
	 * @param App\Models\Category $model
	 */
	public function __construct(Category $model)
	{
		$this->model = $model;
	}

	/**
	 * Get all categories.
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAll($request)
	{	

		 $r = $request;

		 if (isset($r["sort"])){
	        $sort = explode("|",$r["sort"]);
	      }
	       
	      if (isset($r["filter"])) {
	        $categories = $this->model->where('name', 'like', '%' . $r["filter"] . '%')->orderBy( $sort[0] ,$sort[1])->paginate(5);
	      }else if(!isset($r["sort"])){
	           $categories = $this->model->all();
	           return response()->json(compact('categories'));
	      }else{
	        $categories = $this->model->orderBy( $sort[0] ,$sort[1])->paginate(5);
	      }

		return $categories;
	}

	/**
	 * Get category by id.
	 *
	 * @param integer $id
	 *
	 * @return App\Models\Category
	 */
	public function getById($id)
	{
		return $this->model->find($id);
	}

	/**
	 * Create a new Category.
	 *
	 * @param array $request
	 *
	 * @return App\Models\Category
	 */
	public function create(array $request)
	{

      $input = $request;

      if ($input["image"]) {

        
         $img = Image::make($input["image"]);
         $save_path = public_path().'/uploads/categories/';
         $filename = str_random(40).'.jpeg';
         $filepath = $save_path . $filename;
         $url = '/uploads/categories/'. $filename;

         if (!file_exists($save_path)) {
            mkdir($save_path, 666, true);
          }

         $img->save($filepath);
         $input["image"] = $url;

      }else{
         $input["image"] = "";
      }

      $this->model->create($input)->save();

      return response()->json('Success');

	}

	/**
	 * Update a Category.
	 *
	 * @param integer $id
	 * @param array $request
	 *
	 * @return App\Models\Category
	 */
	public function update($id, array $request)
	{	

		$input = $request;
		$category = $this->model->find($id);
      	$category->name = $input['name'];

       if ($input["image"]) {

             $img = Image::make($input["image"]);
             $save_path = public_path().'/uploads/categories/';
             $filename = str_random(40).'.jpeg';
             $filepath = $save_path . $filename;
             $url = '/uploads/categories/'. $filename;

             if (!file_exists($save_path)) {
              mkdir($save_path, 666, true);
            }

             $img->save($filepath);
             $category->image = $url;
      }

      $category->save();
      return response()->json('Success');
	}

	/**
	 * Delete a Category.
	 *
	 * @param integer $id
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		$category = $this->model->find($id);
        $category->delete();
        return response()->json('Success');
	}
}