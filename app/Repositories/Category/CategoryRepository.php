<?php

namespace App\Repositories\Category;

interface CategoryRepository
{
	function getAll($request);

	function getById($id);

	function create(array $request);

	function update($id, array $request);

	function delete($id);
}