<?php

namespace App\Repositories\Post;

interface PostRepository
{
	function getAll($request);

	function getById($id);

	function create(array $request);

	function update($id, array $request);

	function delete($id);
}