<?php

namespace App\Repositories\Post;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use Auth;


class EloquentPost implements PostRepository
{
	/**
	 * @var $model
	 */
	private $model;

	/**
	 * EloquentPost constructor.
	 *
	 * @param App\Models\Post $model
	 */
	public function __construct(Post $model)
	{
		$this->model = $model;
	}

	/**
	 * Get all posts.
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAll($request)
	{	

		  $r = $request;

	      if (isset($r["sort"])){
	        $sort = explode("|",$r["sort"]);
	      }
	       
	      if (isset($r["filter"])) {

	        $posts = Post::where('name', 'like', '%' . $r["filter"] . '%')
	        			 ->orWhere('description', 'like', '%' . $r["filter"] . '%')
	        			 ->orderBy( $sort[0] ,$sort[1])
	        			 ->with(['category','user'])
	        			 ->paginate(5);

	      }else if(!isset($r["sort"])){

	           $posts = Post::paginate(5)->with(['category','user']);

	      }else{

	        $posts = Post::with(['category','user'])
	                  ->orderBy( $sort[0] ,$sort[1])
	                  ->paginate(5);
	        
	      }

	      return $posts;
	}

	/**
	 * Get post by id.
	 *
	 * @param integer $id
	 *
	 * @return App\Models\Post
	 */
	public function getById($id)
	{
		return $this->model->find($id);
	}

	/**
	 * Create a new Post.
	 *
	 * @param array $request
	 *
	 * @return App\Models\Post
	 */
	public function create(array $request)
	{

      	$input = $request;
 		$input['user_id'] = auth()->user()->id;
        Post::create($input);
        return response()->json('Success');

	}

	/**
	 * Update a Post.
	 *
	 * @param integer $id
	 * @param array $request
	 *
	 * @return App\Models\Post
	 */
	public function update($id, array $request)
	{	
		$input = $request;
        $post =$this->model->find($id);
        $post->update($input);
        return response()->json("Success");
	}

	/**
	 * Delete a Post.
	 *
	 * @param integer $id
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		$post = $this->model->find($id);
        $post->delete();
        return response()->json('Success');
	}
}