<?php

namespace App\Models;

// use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  	// use HasApiTokens;
    protected $table = 'categories';
    protected $fillable = ['name','image'];
    public $timestamps = false;
}
