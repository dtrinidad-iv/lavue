<?php

namespace App\Models;

// use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // use HasApiTokens;
    protected $table = 'posts';
    protected $fillable = ['name','description','user_id','category_id'];
    // public $timestamps = false;


    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


}
