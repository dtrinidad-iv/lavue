<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use Response;


class CategoryController extends Controller
{

   /**
   * @var $category
   */
   private $category;


    public function __construct(CategoryRepository $category) 
    {
      $this->category = $category;
    }



    public function index(Request $request)
    {

      $input = $request->all();
      $categories = $this->category->getAll($input);
      return $categories;

    }

   
    public function store(Request $request)
    { 

      $this->validate($request, [
        'name' => 'required'
      ]);

      $input = $request->all();
      $response =  $this->category->create($input);
      return $response;
    }


    public function show($id)
    {

      $category = $this->category->getById($id);
      return response()->json($category);

    }


    public function update(Request $request, $id)
    {

      $this->validate($request, [
        'name' => 'required'
      ]);

      $input = $request->all();
      $response =  $this->category->update($id,$input);
      return $response;


    }

    public function destroy($id)
    {
      $response = $this->category->delete($id);
      return $response;
    }
}
