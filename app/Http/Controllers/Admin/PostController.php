<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Post\PostRepository;
use App\Models\Post;
use App\Models\Category;
use Validator;
use Response;
use Auth;

class PostController extends Controller
{
     

  /**
   * @var $post
   */
   private $post;


    public function __construct(PostRepository $post) 
    {
      $this->post = $post;
    }


    public function index(Request $request)
    {	

      $input = $request->all();
      $posts = $this->post->getAll($input);
      return response()->json($posts);
    }

 
    public function store(Request $request)
    {		

        $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
          'category_id' => 'required'
        ]);

        $input = $request->all();
        $response = $this->post->create($input);
        return $response;
    }

  
    public function show($id)
    {
        $post = $this->post->getById($id);
      	return response()->json($post);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
          'category_id' => 'required'
        ]);

      $input = $request->all();
      $response =  $this->post->update($id,$input);
      return $response;

    }

    public function destroy($id)
    {
        $response = $this->post->delete($id);
        return $response;
    }
}
