<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Traits\Authorizable;
class PageController extends Controller
{
    	
    use Authorizable;
    
    public function __constructor(){
      $this->middleware('auth');
    }

    public function categories(){
      return view('admin.categories');
    }

    public function posts(){
      return view('admin.posts');
    }

 
}
