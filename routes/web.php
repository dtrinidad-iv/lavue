<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'auth'], function () {
	
 	Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
	Route::get('/admin/dashboard', 'Admin\HomeController@index');
	Route::get('categories', 'Admin\PageController@categories')->name('categories.view');
	Route::get('posts', 'Admin\PageController@posts')->name('posts.view');
	// Route::get('categories','Admin\PageController@categories');
	// Route::get('posts','Admin\PageController@posts');

    // Route::resource('posts', 'PostController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});



Route::get('/', function () {
    return view('site.home');
});


Route::auth();

// Route::get('post','PageController@post');